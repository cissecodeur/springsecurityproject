package org.sid.services;

import java.util.ArrayList;
import java.util.Collection;

import org.sid.entities.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private AccountService accountService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser user = accountService.findUserByUsername(username);
			 if(user == null) throw new UsernameNotFoundException(username);	 
			  Collection<GrantedAuthority> authorities = new ArrayList<>();
	          user.getRoles().forEach(roles->{
	          authorities.add(new SimpleGrantedAuthority(roles.getRoleName()));
	         });
			 
		return new User(user.getUserName() , user.getPassword() , authorities);
	}

}
