package org.sid.services;



import java.util.List;

import org.sid.entities.Speaker;
import org.sid.repositories.SpeakerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	
	
	@Autowired 
	private SpeakerRepo speakerRepo;
	
	
	
	@GetMapping("/all")
	public List<Speaker> afficher(){
		return speakerRepo.findAll();
	}
	
	@GetMapping("/all/{id}")
	public Speaker afficherOne(@PathVariable int id){
		return speakerRepo.findById(id);
	}


}
