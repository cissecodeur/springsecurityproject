package org.sid.web;

import org.sid.entities.AppUser;
import org.sid.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AccountRestController {

	@Autowired
	private AccountService accountService;
	
	@PostMapping("/enregistrer")
	public AppUser enregisterUser(@RequestBody RegisterForm userform) {	
		if(!userform.getPassword().equals(userform.getConfirmpassword()))
			throw new RuntimeException("Veuillez confirmer votre mot de passe");	
	AppUser user = accountService.findUserByUsername(userform.getUsername());
	
      	if (user != null)
      		throw new RuntimeException("Ce user existe deja");
	AppUser appUser = new AppUser();
	appUser.setUserName(userform.getUsername());
	appUser.setPassword(userform.getPassword());
    accountService.saveUser(appUser);
    accountService.addRoletoUser(userform.getUsername(), "USER");
    return appUser;
	}
	
	
	
}




