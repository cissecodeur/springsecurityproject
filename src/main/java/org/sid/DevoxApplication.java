package org.sid;

import java.util.stream.Stream;

import org.sid.entities.AppRole;
import org.sid.entities.AppUser;
import org.sid.entities.Task;
import org.sid.repositories.TaskRepository;
import org.sid.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class DevoxApplication implements CommandLineRunner {
	
	@Autowired
	private TaskRepository taskRepository;	
	
	
	@Autowired
	private AccountService accountService;
	
	
	@Bean
	public BCryptPasswordEncoder GetbCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
		
	}

	// Definition d'une securité pour son APi	
	/*
	 * @Configuration static class SecurityConfig extends
	 * GlobalAuthenticationConfigurerAdapter{
	 * 
	 * @Override public void init(AuthenticationManagerBuilder auth) throws
	 * Exception{
	 * 
	 * auth.inMemoryAuthentication()
	 * .withUser("user").password("user").roles("USER"). and()
	 * .withUser("cisse").password("cisse").roles("USER","ADMIN");
	 * 
	 * } }
	 */
	public static void main(String[] args) {
		SpringApplication.run(DevoxApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		accountService.saveUser(new AppUser(null,"admin","12345",null));
		accountService.saveUser(new AppUser(null,"user","12345",null));
		accountService.saveRole(new AppRole(null,"ADMIN"));
		accountService.saveRole(new AppRole(null,"USER"));
		accountService.saveRole(new AppRole(null,"DEVELOPPERS"));
		accountService.saveRole(new AppRole(null,"OPERATORS"));
		accountService.saveRole(new AppRole(null,"COMPTABLES"));
		accountService.saveRole(new AppRole(null,"COMMERCIALES"));
		accountService.addRoletoUser("admin", "ADMIN");
		accountService.addRoletoUser("admin", "USER");
		
		Stream.of("Tache1","Tache2","Tache3").forEach(tache->{
			taskRepository.save(new Task(null,tache));
		});
		
	       taskRepository.findAll().forEach(tache->{
	    	   System.out.println(tache.getTaskName());
	       });
		
	}

}
