package org.sid.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.ManyToAny;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;


@Entity
public class AppUser implements Serializable{
       
	   @Id
	   @GeneratedValue(strategy = GenerationType.IDENTITY)
	   private Long id;   
	   @Column(unique = true) //le username doit etre unique dans la bd
	   private String userName;
	   private String password;	
	   @ManyToMany(fetch = FetchType.EAGER) //charger les roles lors du chargement du user
	   private Collection<AppRole> roles = new ArrayList<AppRole>();
	   
	public AppUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AppUser(Long id, String userName, String password, Collection<AppRole> roles) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.roles = roles;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
    
	 @JsonIgnore
	public String getPassword() {
		return password;
	}
	 
    @JsonSetter
	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<AppRole> getRoles() {
		return roles;
	}

	public void setRoles(Collection<AppRole> roles) {
		this.roles = roles;
	}
	   
	   
	   
	  
}
