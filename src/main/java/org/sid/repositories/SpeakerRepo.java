package org.sid.repositories;

import org.sid.entities.Speaker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SpeakerRepo extends JpaRepository<Speaker, Integer> {

	/*
	 * public Collection<Speaker> findByfirstname(String firstname); public
	 * Collection<Speaker> findBylastname(String lastname);
	 * 
	 * public Speaker findByTwitter(String twitter);
	 */
	
	public Speaker findById(int id);
}
