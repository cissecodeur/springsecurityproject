package org.sid.services;

import org.sid.entities.AppRole;
import org.sid.entities.AppUser;
import org.sid.repositories.RoleRepository;
import org.sid.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	 
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
	
	//Quand un user doit etre ajouter il faut crypter son mot de passe
	@Override
	public AppUser saveUser(AppUser user) {		
		String hashPassword = bCryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(hashPassword);
		return userRepository.save(user) ;
	}

	@Override
	public AppRole saveRole(AppRole role) {	
		return roleRepository.save(role);
	}

	@Override
	public void addRoletoUser(String userName, String roleName) {
		AppRole approle = roleRepository.findByRoleName(roleName);
		AppUser appuser = userRepository.findByUserName(userName);
		appuser.getRoles().add(approle);
	
		
	}

	@Override
	public AppUser findUserByUsername(String userName) {	
		return userRepository.findByUserName(userName);
	}

}
