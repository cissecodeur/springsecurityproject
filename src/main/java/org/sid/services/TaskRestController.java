package org.sid.services;

import java.util.List;
import java.util.Optional;

import org.sid.entities.Task;
import org.sid.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskRestController {
	
	@Autowired
	private TaskRepository taskRepository;

	@GetMapping("/tasks")
	public List<Task> ListTask(){
		return taskRepository.findAll();
		
	}
	
	@GetMapping("/tasks/{id}")
	public Optional<Task> findTaskById(@PathVariable Long id){
		return taskRepository.findById(id);
		
	}
	
	@PostMapping("/tasks")
	public Task AddTask(@RequestBody Task task) {
		return taskRepository.save(task);
	}
}
