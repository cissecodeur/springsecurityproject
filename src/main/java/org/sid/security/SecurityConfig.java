package org.sid.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration // car ce fichier est un fichier de configuration
@EnableWebSecurity //activer la securité web
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired 
	private UserDetailsService userDetailsService; // Interface native a SpringSecurity
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder; // Interface native a SpringSecurity
	
	//definir les utilisateurs et leurs credentials (les utilisateurs sont en memoires)
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		/*
		 * auth.inMemoryAuthentication()
		 * .withUser("admin").password("12345").roles("ADMIN","USER") .and()
		 * .withUser("user").password("1234").roles("USER");
		 */
		
		    auth.userDetailsService(userDetailsService)  // Authentification basée sur user details
		   .passwordEncoder(bCryptPasswordEncoder);
	}
	
	
	// Methode pour definir les droits d'acces
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	
		  http.csrf().disable(); //pour desactiver le synchronize token
		  http.formLogin() ; //formulaire de secure definie par spring-security
		  http.authorizeRequests().antMatchers("/login/**","/enregistrer/**").permitAll(); //tout le monde peut s'enregistrer et s'authentifier
		  http.authorizeRequests().antMatchers(HttpMethod.POST, "/tasks/**").hasAuthority("ADMIN"); //Avoir uniquement le droit admin pour poster
		  http.authorizeRequests().anyRequest().authenticated(); // Definir les droits pour chaque requtes
	}
	

}
